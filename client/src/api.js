const API_URL = 'http://127.0.0.1:3000'

const serialize = function(obj) {
  let str = [];
  for (let p in obj) {
    if (obj.hasOwnProperty(p) && obj[p]) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  }
  return str.join("&");
}

const _get = async (endpoint, query) => {
  let url_path = `${API_URL}/${endpoint}`

  if (query) {
    let query_string = serialize(query)
    url_path = `${url_path}?${query_string}`
  }
  
  const response = await fetch(url_path)
  const content_type = response.headers.get("content-type");

  if(content_type && content_type.indexOf("application/json") !== -1) {
    return response.json()
  }

  return null
}

const resources = {
  list: (query) => _get('resources', query)
}

const tags = {
  list: () => _get('tags')
}

export default { resources, tags }
