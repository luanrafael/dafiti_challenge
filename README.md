Dafiti Challenge
================

### Estrutura
    .
    ├── server                  # source api rest
    ├── client                  # front-end source
    └── README.md


### Requisitos
    
    Linux Ubuntu 16.04
    
    NodeJS 10.15.3 (https://nodejs.org/en/)
    NPM 6.9.0 (https://www.npmjs.com)
    MySQL 5.7 (https://dev.mysql.com/downloads/mysql/)


### Instalação
    
1. Depois de instalar o node, o npm e o mysql será preciso importar o arquivo `server\db.sql` no seu bando de dados, certifique-se de ter um schema chamado **dev_dafiti_challenge**
    
2. Edite o arquivo `server/config/config.json` com as credenciais de acesso ao seu banco

3. instale as dependencias do projeto usando o comando **npm install** dentro da pasta **server**
    

### Start
  ``npm start``
  
  Acesse `http://127.0.0.1:3000`
  

### Testes
  ```npm test```
    
### Documentação

`http://127.0.0.1:3000/api-docs`


### Importação de CSV
Na pasta `server` existem alguns arquivos `.csv` que podem ser usados como base

**importante**: o arquivo csv, deve conter o seguinte header:
`brand;model;color;size;price;collection;department;description;discount;tags;images`

e deve ser separado por `;`


*  as colunas `price` e `discount` são usadas para formar inserir o campo `price_with_discount` 

    
### Possível problema

Se tiver algum problema relacionado ao `mysql sql_mode only_full_group_by` desabilite a funcionalidade com a seguinte instrução:
`mysql > SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));`