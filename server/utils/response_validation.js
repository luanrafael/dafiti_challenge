const handleErrors = (validation_errors) => {
  return {
    error: true,
    messages: validation_errors && Array.isArray(validation_errors) ? validation_errors.map((value, index, arr) => value.message) : ['internal error']
  }
}

module.exports = {
  handleErrors
}