module.exports = {
  toUpperCaseTrim: (value) => value ? String(value).toUpperCase().trim() : value,
  invalidLenMessage: (field, args) => {
    return {
      args,
      msg: `${field} cant be empty or blank and the field ${field} must to have a len between ${args[0]} - ${args[1]}`
    }
  }
}