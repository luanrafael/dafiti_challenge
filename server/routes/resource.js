const express = require('express')
const router = express.Router()

const controllers = require('../controllers')

/**
 * @swagger
 * /resources:
 *    get:
 *      sumary: list all resources
 *      parameters:
 *        - in: query
 *          name: paginate
 *          schema:
 *            type: integer
 *            default: 25
 *          description: number of resources per page
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *            default: 1
 *          description: page number
 *        - in: query
 *          name: color
 *          schema:
 *            type: string
 *          description: color filter - like operation
 *        - in: query
 *          name: brand
 *          schema:
 *            type: string
 *          description: brand filter - like operation
 *        - in: query
 *          name: model
 *          schema:
 *            type: string
 *          description: model filter - like operation
 *        - in: query
 *          name: size
 *          schema:
 *            type: string
 *          description: size filter
 *        - in: query
 *          name: min_price
 *          schema:
 *            type: float
 *          description: min_price filter
 *        - in: query
 *          name: max_price
 *          schema:
 *            type: float
 *          description: max_price filter
 *        - in: query
 *          name: department
 *          type: string
 *          enum: [MALE, FEMALE, KIDS, SHOES]
 *          description: department filter
 *        - in: query
 *          name: tags
 *          schema:
 *            type: array
 *            items:
 *              type: string
 *          description: tags filter
 *          explode: false
 *          style: form
 *      responses:
 *        200:
 *    post:
 *      parameters:
 *      - name: brand
 *        required: true
 *        type: string
 *      - name: model
 *        required: true
 *        type: string      
 *      - name: color
 *        required: true
 *        type: string      
 *      - name: size
 *        required: true
 *        type: string      
 *      - name: collection
 *        type: string  
 *        required: false
 *      - name: price
 *        required: true
 *        type: float      
 *      - name: description
 *        type: string      
 *        required: false
 *      - name: department
 *        type: string
 *        enum: [MALE, FEMALE, KIDS, SHOES]
 *        required: true
 *      - name: discount
 *        required: false
 *        type: float
 *      - name: images
 *        schema:
 *          type: array
 *          itens:
 *            type: string
 *        description: list of images urls
 *      - name: tags
 *        schema:
 *          type: array
 *          itens:
 *            type: string
 *      responses:
 *        201:
 * /resources/{id}:
 *    put:
 *      parameters:
 *      - in: path
 *        name: id
 *        required: true
 *      - name: brand
 *        type: string
 *      - name: model
 *        type: string      
 *      - name: color
 *        type: string      
 *      - name: size
 *        type: string      
 *      - name: collection
 *        type: string      
 *      - name: price
 *        type: float      
 *      - name: description
 *        type: string      
 *      - name: department
 *        type: string
 *        enum: [MALE, FEMALE, KIDS, SHOES]
 *      - name: discount
 *        type: float
 *      - name: images
 *        schema:
 *          type: array
 *          itens:
 *            type: string
 *        description: list of images urls
 *      - name: tags
 *        schema:
 *          type: array
 *          itens:
 *            type: string
 *      responses:
 *        200:
 *    get:
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true 
 *      responses:
 *        200:
 *    delete:
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true 
 *      responses:
 *        200: 
 * /resources/csv_import:
 *    post:
 *      description: csv import - file upload - limitation > 1000 lines
 *      consumes:
 *        - multipart/form-data
 *      parameters:
 *        - in: formData
 *          name: csv
 *          type: file
 *          description: csv to upload max 1000 lines
 *      responses:
 *        201:
 * /resources/csv_import/{import_id}:
 *    get:
 *      description: returns the import logs
 *      parameters:
 *        - in: path
 *          name: import_id
 *          required: true
 *      responses:
 *        200:
 *
 * @swagger
 * /tags:
 *    get:
 *      description: returns the all tags
 *      responses:
 *        200:
 */
router
  .post('/csv_import', controllers.resource.csvImport)
  .get('/csv_import/:id', controllers.resource.importReport)
  .get('/', controllers.resource.list)
  .post('/', controllers.resource.save)
  .put('/:id', controllers.resource.save)
  .get('/:id', controllers.resource.findById)
  .delete('/:id', controllers.resource.destroy)

module.exports = router;
