const express = require('express')
const router = express.Router()

const controllers = require('../controllers')

router
  .get('/', controllers.resource.listTags)

module.exports = router;
