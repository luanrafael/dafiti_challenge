module.exports = {
  resource: require('./resource'),
  image: require('./image'),
  csv_import: require('./csv_import')
}