const csv = require('csvtojson')
const db = require('../models')

const resourceService = require('./resource')
const imageService = require('./image')

const _splitRow = (row_value, tags) => {
  if (row_value && row_value.length > 0) {
    row_value = row_value.replace(/[,\n\t]/gi,';')
    return row_value.split(';')
  }
  return null
}


const buildImport = (rows) => db.csv_import.create({rows, status: 'PENDING'})

const doImport = async (csv_file, cb) => {
  console.log(csv_file)
  const csv_file_path = csv_file.tempFilePath
  let tags_to_import = []

  const options = {
    colParser:{
      'price': 'number',
      'discount': 'number',
      'tags': (item, head, resultRow, row , colIdx) => {
        const arr_tags = _splitRow(item)
        if (arr_tags) {
          tags_to_import = arr_tags.concat(tags_to_import) 
        }
        return arr_tags
      },
      'images':(item, head, resultRow, row , colIdx) => {
        return _splitRow(item)
      }
    },
    delimiter: ';',
    escape: '"'
  }  
  
  const logs = []

  function log () {
    if (!arguments) return;

    if (arguments.length === 0) return;
    const messages = []
    for (let index = 3; index < arguments.length; index++) {
      messages.push(arguments[index])
    }

    const data_log = {
      type: String(arguments['0']).toUpperCase(),
      timestamp: Date.now(),
      row_number: arguments['1'] || null,
      row: arguments['2'] || null,
      messages
    }

    logs.push(data_log)
  }

  log('INFO', 0, '', 'STARTING')

  const csv_data = await csv(options).fromFile(csv_file_path);
  
  if (csv_data.length < 1) {
    return cb({error: true, message: 'invalid csv size, it must to have at least 2 lines: header and one data line'})
  }

  if (csv_data.length > 1000) {
    return cb({error: true, message: 'invalid csv size, file cant have more than 1000 lines', line_count: csv_data.length})
  }

  const pending_csv_import = await buildImport(csv_data.length);
  
  cb(pending_csv_import)

  log('INFO', 0, '', 'CSV IMPORT CREATED', `ID: ${pending_csv_import.id}`)

  const tags = [...new Set(tags_to_import)]
  
  log('INFO', 0, '', 'SAVING TAGS', tags)

  const created_tags = await resourceService.saveTags(tags)

  log('INFO', 0, '', 'NEW TAGS', created_tags)

  let transaction
  
  try {
    transaction = await db.sequelize.transaction()
  
    log('INFO', 0, '', 'STARTING TRANSACTION')

    for (let i = 0; i < csv_data.length; i++) {
      
      const data = csv_data[i];
      log('INFO', i + 1, data, 'PROCESSING')
      const resource = await resourceService.save(data, null, transaction, {only_validate: true})
      if (resource.error) {
        log('ERROR', i + 1, data, resource.messages)
        throw new Error('Import Error')
      } else {
        log('INFO', i + 1, data, 'SAVING IMAGES')
        await imageService.save(data.images, resource.id, transaction)
        log('INFO', i + 1, data, 'DONE IMAGES')
      }
    }

    await transaction.commit();
    log('INFO', 0, '', 'CSV IMPORT - DONE')
    pending_csv_import.status = 'DONE'

  } catch (err) {
    console.error(err)
    //log('ERROR', 0, '', err)
    log('INFO', 0, '', 'CSV IMPORT - ROLLBACK')
    if (err) await transaction.rollback();
    log('INFO', 0, '', 'CSV IMPORT - ROLLBACK - DONE')
    pending_csv_import.status = 'ERROR'

  }

  await db.log_csv_import.create({id: pending_csv_import.id, log: logs})
  await pending_csv_import.save();

  return pending_csv_import;
}

const importReport = (id) => db.csv_import.findByPk(id, {include: ['logs']})

module.exports = {
  importReport,
  doImport
}