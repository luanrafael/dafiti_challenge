const db = require('../models')
const utils = require('../utils')
const Op  = db.Sequelize.Op

const save = async (resource, resource_id, transaction) => {
  
  let resource_instance

  resource.price_with_discount = 0

  if (resource_id) {
    const found_resource = await findById(resource_id, transaction)
    if (!found_resource) return null

    resource_instance = db.resource.build(found_resource.dataValues, { isNewRecord: false });
    const attributes = Object.keys(resource)
    attributes.forEach( attr => resource_instance[attr] = resource[attr])
  } else {
    resource_instance = db.resource.build(resource);
  }


  try {
    await resource_instance.validate()
  } catch (validation_exection) {
    return utils.response_validation.handleErrors(validation_exection.errors)
  }

  const saved_resource = await resource_instance.save({transaction})
  if (resource.tags) {
    resource.tags = resource.tags.filter( t => t )
    await saveTags(resource.tags, transaction)
    await resource_instance.setTags(resource.tags, {transaction})
  }
  
  return saved_resource
}

const findById = (id, transaction) => db.resource.findByPk(id, {include: ['images'], transaction})

const list = async (query) => {

  if (!query) {
    query = {page: 1, paginate: 25}
  }
  const _like = (v) => { return { [Op.like]: `%${v}%` } }
  const _lte = (v) => { return { [Op.lte]: v.replace(',', '.')} }
  const _gte = (v) => { return { [Op.gte]: v.replace(',', '.')} }
  const _eq = (v) => { return { [Op.eq]: v} }
  const _eqNumber = (v) => { return { [Op.eq]: v.replace(',', '.')} }
  const _in = (v) => { return { [Op.in]: v} }

  const attribute_filters = [
    {
      attr: 'color',
      fn: _like,
    },
    {
      attr: 'brand',
      fn: _like,
    },
    {
      attr: 'model',
      fn: _like,  
    },
    {
      attr: 'size',
      fn: _eq,
    },
    {
      attr: 'min_price',
      fn: _gte,
      key: 'price'
    },
    {
      attr: 'max_price',
      fn: _lte,
      key: 'price'
    },
    {
      attr: 'department',
      fn: _eq
    },
    {
      attr: 'collection',
      fn: _like
    }
  ]

  const conditions = {}

  if(query.tags && !Array.isArray(query.tags) && query.tags.length > 0){
    query.tags = query.tags.split(',')
  }

  const tag_filter = query.tags ? {tag: _in(query.tags) } : {}
  const required_tag  = query.tags && query.tags.length > 0 ? true : false
  attribute_filters.forEach( schema_attr => {
    if (query[schema_attr.attr]) {
      const key = schema_attr.key || schema_attr.attr
      if (conditions[key]) {
        conditions[key] = Object.assign(conditions[key],schema_attr.fn(query[schema_attr.attr]))
      } else {
        conditions[key] = schema_attr.fn(query[schema_attr.attr])
      }
    }
  })

  query.page = isNaN(Number(query.page)) ? 1 : Number(query.page)
  query.paginate = isNaN(Number(query.paginate)) ? 25 : Number(query.paginate)

  const options = {
    page: query.page,
    limit: query.paginate,
    where: conditions,
    include: [
      {
        model: db.tag,
        as:'tags',
        where: tag_filter,
        attributes: ['tag'],
        through: {
          attributes: []
        },
        required: required_tag,
        duplicating: false
      }
    ],
    group: ['resource.pk_resource_id']
  }
  
  const pagination = await db.resource.paginate(options)

  return pagination

}
const deleteById = (id) => db.resource.destroy({where: { id }})

const saveTags = async (resource_tags, transaction) => {
  if (!resource_tags) return
  
  const unique_tags = [...new Set(resource_tags)]
  const tags = await db.tag.findAll({where: {tag: unique_tags}, transaction})
  const unsaved_tags = unique_tags.filter( new_tag => !tags.find( t => t.tag.toUpperCase() === utils.string.toUpperCaseTrim(new_tag) ) )
  const new_tags = []

  unsaved_tags.forEach( t => {
    new_tags.push({tag: t})
  })

  return db.tag.bulkCreate(new_tags, {transaction})
}

const listTags = () => db.tag.findAll({attributes: ['tag']})

module.exports = {
  save,
  list,
  findById,
  deleteById,
  saveTags,
  listTags
}