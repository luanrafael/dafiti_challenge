const db = require('../models')
const resourceService = require('./resource')

const save = (images, resource_id, transcation) => {
  
  if (!images) return null

  const resource = resourceService.findById(resource_id, transcation)

  if (!resource) return null
  
  if (!resource.images) resource.images = []
  
  images.forEach(image => image.resource_id = resource_id);

  const filtered_images = images.filter( image => {
    image.resource_id = resource_id
    if(image.url && !resource.images.find( resource_image => resource_image.url === image.url )) {
      return image
    }
  })

  return db.image.bulkCreate(filtered_images, {transcation})
}

const deleteById = (id) => db.image.destroy({where: {id}})

module.exports = {
  save,
  deleteById
}