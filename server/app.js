const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fileUpload = require('express-fileupload');
const helmet = require('helmet');
const routes = require('./routes');

var app = express();
app.use(helmet());
app.disable('x-powered-by');

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'DELETE, GET, POST, PUT, OPTIONS')
  res.header('Access-Control-Allow-Credentials', true)
  if ('OPTIONS' == req.method) {
    res.sendStatus(200);
  } else {
    next();
  }
})

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const FILE_SIZE_LIMIT = 1 * 1024 * 1024

app.use(fileUpload({
  useTempFiles : true,
  tempFileDir : '/tmp/',
  limits: {
    fileSize: FILE_SIZE_LIMIT
  },
  abortOnLimit: true,
  limitHandler: (req, res, next ) => {
    res.status(413).send({error: true, message: 'File size limit has been reached', limit: FILE_SIZE_LIMIT })
    next(new Error('File size limit has been reached'))
  }
}));

const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const options = {
  swaggerDefinition: {
    info: {
      title: 'Resource API',
      version: '1.0.0',
      description: 'Resource API',
      author: 'Luan Rafael C. Pinheiro <luanrafaelcastor@gmail.com>'
    },
  },
  // List of files to be processes. You can also set globs './routes/*.js'
  apis: ['./routes/resource.js','./routes/tag.js'],
};

const specs = swaggerJSDoc(options);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs, {explorer: true}));
app.use('/resources', routes.resource);
app.use('/tags', routes.tag);

module.exports = app;
