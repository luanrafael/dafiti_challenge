const services = require('../services')

const _response = async (req, res, promise, status_code, cb) => {
  
  if (!status_code) status_code = 200

  if (req.method === 'POST') status_code = 201

  try {
    const response = await promise;
    if (response === null) status_code = 404
    
    if (response.error) {
      return res.status(400).send(response)
    }
    
    if (cb) {
      cb(response)
    }
    return res.status(status_code).send(response)
  } catch (error) {
    console.error(error)
    return res.status(500).send({error: true, message: 'Internal Error, please contact the administrator'})
  }

}

const save = async (req, res) => {

  _response(req, res, services.resource.save(req.body, req.params.id), (resource) => {
    if (!resource.error) {
      services.image.save(req.body.images,resource.id || req.params.id)
    }
  })

}

const list = async (req, res) => _response(req, res, services.resource.list(req.query))

const destroy = (req, res) => _response(req, res, services.resource.destroy(req.params.id))

const findById = (req, res) => _response(req, res, services.resource.findById(req.params.id))

const csvImport = async (req, res) => {
  try {
    await services.csv_import.doImport(req.files.csv, (pending_csv_import) => {
      if (pending_csv_import.error) {
        return res.status(400).send(pending_csv_import)
      }
      return res.status(201).send(pending_csv_import)
    })
  } catch (error) {
    console.error(error)
    return res.status(500).send({error: true, message: 'Internal Error, please contact the administrator'})
  }

}

const importReport = async (req, res) => {
  const response = await services.csv_import.importReport(req.params.id)
  res.status(200).send(response)
}

const listTags = async (req, res) => _response(req, res, services.resource.listTags())

module.exports = {
  save,
  list,
  destroy,
  findById,
  csvImport,
  importReport,
  listTags
}