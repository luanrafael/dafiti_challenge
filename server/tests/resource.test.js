const chai = require('chai')
const services = require('../services')
const db = require('../models')

const expect = chai.expect


before(function () {
  this.timeout(20000)
  return db.sequelize.sync({force: true})
})

describe('Testing Resource CRUD Operations', function() {
  this.timeout(10000)
  
  let resource_id

  it('CREATE - RESOURCE - WITH ERROR', async () => {
    const resource_with_error = await services.resource.save({brand: 'cavaleira', color: 'RED'})
    expect(resource_with_error.error).to.equal(true)
  })

  it('CREATE - RESOURCE - OK', async () => {
    const resource_ok = await services.resource.save({model: 'model 1', brand: 'cavaleira', color: 'RED'})
    expect(resource_ok).not.null
    expect(resource_ok.error).to.undefined
    resource_id = resource_ok.id
  })

  it('CREATE - IMAGE RESOURCE - OK', async () => {
    const images = [
      {url: 'http://google.com'},
      {url: 'http://youtube.com'}
    ]
    const image_response = await services.image.save(images, resource_id)
    expect(image_response).not.null
  })
  
  it('READ  - LIST ALL RESOURCES', async () => {
    const resources = await services.resource.list()
    expect(resources.docs.length).to.equal(1)
    resource_id = resources.docs[0].id
  })
  
  it('READ  - FIND RESOURCE BY ID', async () => {
    const resource = await services.resource.findById(resource_id)
    expect(resource.id).to.equal(resource_id)
    expect(resource.images).not.to.equals(undefined)
  })
  let image_id 
  it('READ  - FIND RESOURCE BY ID WITH IMAGES', async () => {
    const resource = await services.resource.findById(resource_id)
    expect(resource.id).to.equal(resource_id)
    expect(resource.images.length).to.equals(2)
    image_id = resource.images[0].id
  })

  it('DELETE - IMAGE RESOURCE - OK', async () => {
    const image_deletion_response = await services.image.deleteById(image_id)
    expect(image_deletion_response).to.equals(1)
  })

  it('UPDATE - RESOURCE FIND INSTANCE - OK', async () => {
    const resources = await services.resource.list()
    const first_resource = resources.docs[0].dataValues
    first_resource.price = 10.50
    const resource_update_ok = await services.resource.save(first_resource, first_resource.id)
    expect(resource_update_ok).not.null
    const updated_resource = await services.resource.findById(first_resource.id)
    expect(updated_resource.price).to.equals(10.5)
  })

  it('UPDATE - RESOURCE - OK', async () => {
    const updated_resource = await services.resource.save({price: 11.50}, resource_id)
    expect(updated_resource.price).to.equals(11.5)
  })

  it('DELETE - RESOURCE - OK', async () => {
    const deletion_result = await services.resource.deleteById(resource_id)
    expect(deletion_result).to.equals(1)
    const resources = await services.resource.list()
    expect(resources.docs.length).to.equals(0)
    const updated_resource = await services.resource.save({price: 11.50}, resource_id)
    expect(updated_resource).to.null
  })

})
