const chai = require('chai')
const services = require('../services')
const db = require('../models')

const expect = chai.expect


describe('Testing TAG', function() {
  this.timeout(10000)

  it('CREATE - RESOURCE - OK', async () => {
    const resource_ok = await services.resource.save({model: 'model 1', brand: 'cavaleira', color: 'RED', tags: ['BERMUDA', 'ELASTANO']})
    expect(resource_ok).not.null
    resource_id = resource_ok.id
  })

  it('CREATE - RESOURCE 2 - OK', async () => {
    const resource_ok = await services.resource.save({model: 'model 2', brand: 'cavaleira', color: 'RED', tags: ['BERMUDA', 'JEANS']})
    expect(resource_ok).not.null
    resource_id = resource_ok.id
  })

})
