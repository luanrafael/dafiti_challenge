
module.exports = (sequelize, DataTypes) => {

  return sequelize.define('log_csv_import', {
    id: {
      field: 'fk_csv_import',
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    log: {
      type: DataTypes.JSON,
    }
  }, 
  { 
    sequelize, 
    modelName: 'log_csv_import',
		timestamps: true
  }
)}