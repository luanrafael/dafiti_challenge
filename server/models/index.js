'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

const sequelizePaginate = (model) => {
  model.paginate =  async (options) => {
    if (!options) {
      options = {}
    }
    
    options.page = options.page && !isNaN(Number(options.page)) ? Number(options.page) : 1 
    options.limit = options.limit && !isNaN(Number(options.limit)) ? Number(options.limit) : 25

    options.subQuery=false;
    if (options.page > 1) {
      options.offset = (options.page - 1) * options.limit
    }
    
    const result = await model.findAndCountAll(options);
    const docs = result.rows;
    const total = result.count.length;
    const pages = Math.ceil(total/options.limit);
    return {docs, total, pages, page: options.page, count: options.page, limit: options.limit}
  }
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
    sequelizePaginate(model);
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.resource.hasMany(db.image, {foreignKey: 'resource_id', as: 'images'});
db.image.belongsTo(db.resource, {foreignKey: 'resource_id', as: 'resource'});

db.resource.belongsToMany(db.tag, {
  through: {
    model: db.resource_x_tag,
    unique: false
  },
  foreignKey: 'resource',
  constraints: false,
  as: 'tags'
});

db.tag.belongsToMany(db.resource, {
  through: {
    model: db.resource_x_tag,
    unique: false,
  },
  foreignKey: 'tag',
  constraints: false,
  as: 'resources'
});

db.csv_import.hasOne(db.log_csv_import, {foreignKey: 'id', as: 'logs'})

module.exports = db;
