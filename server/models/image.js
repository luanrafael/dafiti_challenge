
module.exports = (sequelize, DataTypes) => {

  return sequelize.define('image', {
    id: {
      field: 'pk_image_id',
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    resource_id: {
      field: 'fk_resource_id',
      type: DataTypes.UUID,
      allowNull: false
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false
    }
  },
  { 
    sequelize,
    modelName: 'image',
		timestamps: true
  }
)}