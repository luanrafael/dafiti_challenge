
const stringUtils = require('../utils').string

module.exports = (sequelize, DataTypes) => {

  return sequelize.define('resource', {
    id: {
      field: 'pk_resource_id',
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    brand: {
      type: DataTypes.STRING(56),
      set (value) {
        this.setDataValue('brand', stringUtils.toUpperCaseTrim(value))
      },
      validate: {
        len: stringUtils.invalidLenMessage('resource.brand', [1, 56])
      },
      allowNull: false
    },
    model: {
      type: DataTypes.STRING(56),
      set (value) {
        this.setDataValue('model', stringUtils.toUpperCaseTrim(value))
      },
      validate: {
        len: stringUtils.invalidLenMessage('resource.brand', [1, 56])
      },
      allowNull: false
    },
    color: {
      type: DataTypes.STRING(56),
      set (value) {
        this.setDataValue('color', stringUtils.toUpperCaseTrim(value))
      },
      validate: {
        len: stringUtils.invalidLenMessage('resource.brand', [0, 56])
      }
    },
    size: {
      type: DataTypes.STRING(10),
      set (value) {
        this.setDataValue('size', stringUtils.toUpperCaseTrim(value))
      },
      validate: {
        len: stringUtils.invalidLenMessage('resource.brand', [1, 10])
      }
    },
    collection: {
      type: DataTypes.STRING(56)
    },
    price: {
      type: DataTypes.DECIMAL(5,2),
      get () {
        const value = this.getDataValue('price')
        return value ? Number(value) : 0.0
      },
      validate: {
        min: 0,
        max: 99999
      }
    },
    department: {
      type: DataTypes.ENUM("MALE", "FEMALE", "KIDS", "SHOES"),
      validate: {
        customIsIn (value) {
          if(["MALE", "FEMALE", "KIDS", "SHOES"].indexOf(String(value)) === -1) {
            throw new Error('resource.department must to be one of those options ["MALE", "FEMALE", "KIDS", "SHOES"]');
          }
        }
      }
    },
    description: {
      type: DataTypes.STRING(256)
    },
    discount: {
      type: DataTypes.DECIMAL(2,2),
      validate: {
        min: 0,
        max: 1
      }
    },
    price_with_discount: {
      type: DataTypes.DECIMAL(5,2),
      set () {
        const price = Number(this.getDataValue('price') || 0)
        const discount = 1 - Number(this.getDataValue('discount') || 0)
        this.setDataValue('price_with_discount', price * discount)
      }
    }
  }, 
  { 
    sequelize, 
    modelName: 'resource',
		timestamps: true
  }
)}