
module.exports = (sequelize, DataTypes) => {

  return sequelize.define('resource_x_tag', {
    tag: {
      field: 'fk_tag',
      type: DataTypes.STRING(32),
      primaryKey: true
    },
    resource: {
      field: 'fk_resource_id',
      type: DataTypes.UUID,
      primaryKey: true
    }
  },
  { 
    sequelize,
    modelName: 'resource_x_tag',
		timestamps: true
  }
)}