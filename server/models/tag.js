const stringUtils = require('../utils').string

module.exports = (sequelize, DataTypes) => {

  return sequelize.define('tag', {
    tag: {
      field: 'pk_tag',
      type: DataTypes.STRING(32),
      primaryKey: true,
      set (value) {
        this.setDataValue('tag', stringUtils.toUpperCaseTrim(value))
      }
    }
  },
  { 
    sequelize,
    modelName: 'tag',
		timestamps: true
  }
)}