

module.exports = (sequelize, DataTypes) => {

  return sequelize.define('csv_import', {
    id: {
      field: 'pk_csv_import',
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    rows: {
      type: DataTypes.INTEGER(4),
    },
    status: {
      type: DataTypes.ENUM('PENDING', 'DONE', 'ERROR'),
    }
  }, 
  { 
    sequelize, 
    modelName: 'csv_import',
		timestamps: true
  }
)}